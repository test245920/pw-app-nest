import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseFilters,
  ParseUUIDPipe, UsePipes
} from "@nestjs/common";
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserNotFoundException } from './exceptions/user-not-found.exception';
import { HttpExceptionFilter } from './exceptions/http-exception.filter';
import { UserByIdPipe } from "./pipes/user-by-id.pipe";

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post()
  create(@Body() createUserDto: CreateUserDto) {
    return this.usersService.create(createUserDto);
  }

  @Get()
  @UseFilters(new HttpExceptionFilter())
  findAll() {
    throw new UserNotFoundException();
    return this.usersService.findAll();
  }

  @Get(':uuid')
  /* findOne(
    @Param('uuid', new ParseUUIDPipe(), new UserByIdPipe()) uuid: string,
  )*/
  @UsePipes(new ParseUUIDPipe(), new UserByIdPipe())
  findOne(@Param('uuid') uuid: string) {
    return this.usersService.findOne(uuid);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
    return this.usersService.update(+id, updateUserDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.usersService.remove(+id);
  }
}
