import {
  PipeTransform,
  Injectable,
  ArgumentMetadata,
  BadRequestException,
} from '@nestjs/common';

@Injectable()
export class UserByIdPipe implements PipeTransform<string, string> {
  transform(value: string, metadata: ArgumentMetadata): string /*number*/ {
    const val = parseInt(value, 10);
    console.log(val, metadata, value);
    if (isNaN(val)) {
      throw new BadRequestException('Validation failed');
    }
    // return val;
    return value;
  }
}
