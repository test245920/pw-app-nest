import { IsEmail, IsString, Min } from 'class-validator';

export class CreateUserDto {
  @IsString()
  name: string;

  @IsEmail()
  email: string;

  @IsString()
  @Min(6)
  password: string;

  @IsString()
  @Min(6)
  password_confirmation: string;
}
